package com.epam.multithreading.parser;

import com.epam.multithreading.exception.InvalidPathException;
import com.epam.multithreading.ship.LoadingType;
import com.epam.multithreading.ship.Ship;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class JsonShipsParserTest {
    private final ShipsParser parser = new JsonShipsParser();
    private static final String FILE_PATH = "src/test/resources/ships.json";
    private static final String IVALID_FILE_PATH = "src/test/resources/qwe.json";

    @Test
    public void shouldParseJsonFile() throws InvalidPathException {
        //given
        //when
        List<Ship> ships = parser.parse(FILE_PATH);
        //then
        Assert.assertEquals(10, ships.size());

        Ship first = ships.get(0);
        Assert.assertEquals("1st", first.getName());
        Assert.assertEquals(40, first.getDeadweight());
        Assert.assertEquals(LoadingType.UNLOAD_LOAD, first.getLoadingType());
        Assert.assertEquals(40, first.getContainersWeight());

        Ship tenth = ships.get(9);
        Assert.assertEquals("10th", tenth.getName());
        Assert.assertEquals(40, tenth.getDeadweight());
        Assert.assertEquals(LoadingType.LOAD, tenth.getLoadingType());
        Assert.assertEquals(20, tenth.getContainersWeight());

    }
    @Test(expected = InvalidPathException.class)
    public void shouldThrowInvalidPathException() throws InvalidPathException {
        //given
        //when
        parser.parse(IVALID_FILE_PATH);
    }
}
