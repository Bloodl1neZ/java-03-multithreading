package com.epam.multithreading.exception;

public class InvalidPathException extends Exception {
    public InvalidPathException(String path, Throwable cause) {
        super(path, cause);
    }
}
