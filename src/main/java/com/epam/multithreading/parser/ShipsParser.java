package com.epam.multithreading.parser;

import com.epam.multithreading.exception.InvalidPathException;
import com.epam.multithreading.ship.Ship;

import java.util.List;

public interface ShipsParser {
    List<Ship> parse(String path) throws InvalidPathException;
}
