package com.epam.multithreading.ship;

import java.util.ArrayDeque;
import java.util.Queue;

import com.epam.multithreading.container.Container;
import com.epam.multithreading.port.Docks;
import com.epam.multithreading.port.Port;
import org.apache.logging.log4j.*;

public class Ship implements Runnable {
    private static final Logger LOGGER = LogManager.getLogger(Ship.class);

    private Queue<Container> containers;
    private long deadweight;
    private String name;
    private LoadingType loadingType;

    public Ship(String name, long deadweight, LoadingType loadingType) {
        this.name = name;
        this.deadweight = deadweight;
        this.loadingType = loadingType;
        containers = new ArrayDeque<>();
    }

    public long getDeadweight() {
        return deadweight;
    }

    public LoadingType getLoadingType() {
        return loadingType;
    }

    public String getName() {
        return name;
    }

    public long getContainersWeight() {
        return calculateWeight(containers);
    }

    @Override
    public void run() {
        Port port = Port.getInstance();
        boolean done = false;

        while(!done) {
            Docks docks = port.getDocks();
            LOGGER.info(name + " got docks: #" + docks.getId());

            switch (loadingType) {
                case LOAD:
                    done = loadShip(docks);
                    break;
                case UNLOAD:
                    done = unloadShip(docks);
                    break;
                case UNLOAD_LOAD:
                    boolean unloaded = unloadShip(docks);
                    if(!unloaded) {
                        break;
                    }
                    boolean loaded = loadShip(docks);
                    if(loaded) {
                        done = true;
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Not supported argument");
            }
            port.returnDocks(docks);
        }
        LOGGER.info(name + " is done");
    }

    private long calculateWeight(Queue<Container> containers) {
        long currentWeight = 0;
        for(Container container : containers) {
            currentWeight += container.getWeight();
        }
        return currentWeight;
    }

    private boolean loadShip(Docks docks) {
        long availableWeight = deadweight - calculateWeight(containers);
        Queue<Container> takingContainers = docks.takeContainers(availableWeight, name);
        if(takingContainers != null) {
            containers.addAll(takingContainers);
            LOGGER.info(name + " loaded in docks #" + docks.getId());
        }
        return takingContainers != null;
    }

    private boolean unloadShip(Docks docks) {
        if(containers.isEmpty()) {
            return true;
        }

        boolean unload = docks.addContainers(containers, name);
        if(unload) {
            LOGGER.info(name + " unloaded in docks #" + docks.getId());
        }
        return unload;
    }
}
