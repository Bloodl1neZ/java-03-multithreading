package com.epam.multithreading.ship;

public enum LoadingType {
    LOAD,
    UNLOAD,
    UNLOAD_LOAD
}
