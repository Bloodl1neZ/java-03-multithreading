package com.epam.multithreading.port;

import com.epam.multithreading.container.Container;

import java.util.*;

public class Docks {
    private List<Container> containers;
    private long capacity;
    private long id;
    private Map<String, List<Long>> shipGivenContainers = new HashMap<>();

    public Docks(long capacity, long id) {
        this.capacity = capacity;
        this.id = id;
        containers = new ArrayList<>();
    }

    public boolean addContainers(Queue<Container> incomingContainers, String shipName) {
        registerShip(shipName);
        if(!isPossibleToAddContainers(incomingContainers)) {
            return false;
        }
        List<Long> containersIdsGivenByThisShip = shipGivenContainers.get(shipName);

        while(!incomingContainers.isEmpty()) {
            Container incomingContainer = incomingContainers.poll();
            containers.add(incomingContainer);
            long incomingContainerId = incomingContainer.getId();
            containersIdsGivenByThisShip.add(incomingContainerId);
        }
        return true;
    }

    private boolean isPossibleToAddContainers(Queue<Container> incomingContainers) {
        long incomingContainersWeight = calculateWeight(incomingContainers);
        long docksContainersWeight = calculateWeight(containers);
        return incomingContainersWeight + docksContainersWeight <= capacity;
    }

    public Queue<Container> takeContainers(long shipCapacity, String shipName) {
        registerShip(shipName);
        if(calculateWeightWithoutGivenContainers(shipName) == 0) {
            return null;
        }
        Queue<Container> takingContainers = new ArrayDeque<>();
        for(int i = 0; i < containers.size(); i++) {
            Container container = containers.get(i);
            if(!isLoadedThisContainer(shipName, container.getId())
                    && calculateWeight(takingContainers) + container.getWeight() <= shipCapacity) {
                takingContainers.add(container);
                containers.remove(i--);
            }
        }
        return takingContainers;
    }

    private long calculateWeightWithoutGivenContainers(String shipName) {
        long totalWeightWithoutGivenContainers = calculateWeight(containers);
        List<Long> givenContainersIds = shipGivenContainers.get(shipName);
        for(Container container : containers) {
            long containerId = container.getId();
            if(givenContainersIds.contains(containerId)) {
                totalWeightWithoutGivenContainers -= container.getWeight();
            }
        }
        return totalWeightWithoutGivenContainers;
    }

    private long calculateWeight(Collection<Container> containers) {
        long currentWeight = 0;
        for(Container container : containers) {
            currentWeight += container.getWeight();
        }
        return currentWeight;
    }

    private boolean isLoadedThisContainer(String shipName, long id) {
        List<Long> loadedIds = shipGivenContainers.get(shipName);
        return loadedIds.contains(id);
    }

    private void registerShip(String shipName) {
        if(shipGivenContainers.get(shipName) == null) {
            shipGivenContainers.put(shipName, new ArrayList<>());
        }
    }

    public long getId() {
        return id;
    }
}
