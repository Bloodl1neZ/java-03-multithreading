package com.epam.multithreading;

import com.epam.multithreading.exception.InvalidPathException;
import com.epam.multithreading.parser.JsonShipsParser;
import com.epam.multithreading.parser.ShipsParser;
import com.epam.multithreading.port.Port;
import com.epam.multithreading.ship.Ship;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Runner {
    private static final Logger LOGGER = LogManager.getLogger(Runner.class);
    private static final String FILE_PATH = "src/test/resources/ships.json";

    public static void main(String[] args) {
        Runner runner = new Runner();
        runner.execute();
    }

    private void execute() {
        ShipsParser shipsParser = new JsonShipsParser();
        try {
            List<Ship> ships = shipsParser.parse(FILE_PATH);
            List<Thread> shipThreads = createShipThreads(ships);
            startThreads(shipThreads);
        } catch (InvalidPathException e) {
            LOGGER.error(e);
        }
    }
    private void startThreads(List<Thread> threads) {
        int docksAmount = Port.getDocksAmount();
        ExecutorService service = Executors.newFixedThreadPool(docksAmount);
        threads.forEach( o -> service.submit(o) );
        service.shutdown();
    }

    private List<Thread> createShipThreads(List<Ship> ships) {
        List<Thread> threads = new ArrayList<>();
        ships.forEach( o -> threads.add(new Thread(o)) );
        return threads;
    }
}
